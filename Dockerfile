FROM golang:1.19-buster as builder

ARG VIPS_VERSION="8.6.5"

RUN wget https://github.com/libvips/libvips/releases/download/v${VIPS_VERSION}/vips-${VIPS_VERSION}.tar.gz
RUN apt update && apt install -y automake pkg-config libglib2.0-dev gobject-introspection libxml2-dev libexpat-dev libjpeg-dev libwebp-dev libpng-dev libtiff-dev libexif-dev
RUN tar -xf vips-${VIPS_VERSION}.tar.gz && cd vips-${VIPS_VERSION} && ./configure && make -j 8 && make install && ldconfig && cd .. && rm -rf vips-${VIPS_VERSION}
